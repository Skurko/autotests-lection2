﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using HttpRequestHelper;
using Newtonsoft.Json;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace AutoLection2
{
    [TestFixture]
    public class UnitTest1
    {
        [Test]
        public void TestMethod1()
        {
            var request = new GetRequest("http://uk-autoqa01/wsc-test/ContactsService/service.asmx/GetCount");
            var response = request.GetResponse();
            var res = response.StringData();
            var countBeforeTest = JsonConvert.DeserializeObject<ResponseCount>(res);
            
            var addResult = AddContacts();

            request = new GetRequest("http://uk-autoqa01/wsc-test/ContactsService/service.asmx/GetCount");
            response = request.GetResponse();
            res = response.StringData();
            var countAfterTest = JsonConvert.DeserializeObject<ResponseCount>(res);

            Assert.That(countAfterTest.count, Is.EqualTo(countBeforeTest.count + 1));
            
            var getContactsResponse = GetContacts(countAfterTest.count);
            res = GetContactsResponse(getContactsResponse);
            var contacts = JsonConvert.DeserializeObject<ResponseContacts>(res);

            var lastContact = contacts.contacts.Last();

            Assert.That(contacts.contacts.Length, Is.EqualTo(countAfterTest.count));
            Assert.That(lastContact.Id, Is.EqualTo(countAfterTest.count));
            Assert.That(lastContact.Name, Is.EqualTo("TestTestTest"));
            Assert.That(lastContact.Phone, Is.EqualTo("123-45-67"));
        }

        private static WebResponse AddContacts()
        {
            var request = new PostRequest("http://uk-autoqa01/wsc-test/ContactsService/service.asmx/AddContact");

            const string dataStr = "name=TestTestTest&phone=123-45-67";
            var data = Encoding.ASCII.GetBytes(dataStr);
            var req = request.Instance;
            req.ContentLength = data.Length;
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            using (var stream = req.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            return req.GetResponse();
        }

        private static WebResponse GetContacts(int count)
        {
            var request = new PostRequest("http://uk-autoqa01/wsc-test/ContactsService/service.asmx/GetContacts");

            string dataStr = "count=" + count;
            var data = Encoding.ASCII.GetBytes(dataStr);
            var req = request.Instance;
            req.ContentLength = data.Length;
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            using (var stream = req.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            return req.GetResponse();
        }

        private static string GetContactsResponse(WebResponse response)
        {
            string stringResponse;

            using (var stream = response.GetResponseStream())
            {
                var reader = new StreamReader(stream);
                stringResponse = reader.ReadToEnd();
                reader.Close();
            }
            return stringResponse;
        }

        private class ResponseCount
        {
            public int responseCode;
            public int count;
        }

        private class Contact
        {
            public long Id;
            public string Phone;
            public string Name;
        }

        private class ResponseContacts
        {
            public int responseCode;
            public Contact[] contacts;
        }
    }
}
